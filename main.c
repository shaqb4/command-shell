#define _WITH_GETLINE
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <ctype.h>
#include <sys/wait.h>
#include "hashtable.h"

const int SEP = ' ' | 128;
const int SQT = '\'' | 128;
const int DQT = '\"' | 128;
const int BST = '\\' | 128;
const int DOL = '$' | 128;
const int EQ = '=' | 128;
const int LPAR = '(' | 128;
const int RPAR = ')' | 128;
const int AMP = '&' | 128;

//Take a list of char*/strings and starting at the string indicated by param start, combine them into one string seperated by SEP.
//Retrun the string.
char* stringifyArgs(char** args, int start, int numArgs)
{
	char* str = NULL;
	char sep[] = {SEP, '\0'}; //
	int size = 1, i;
	//calculate how many characters will be in the final string
	for (i = start; i < numArgs; i++)
	{
		size += strlen(args[i]);
		if (i < numArgs-1)
			size++;//for spaces at the end
	}
	str = (char*)malloc(sizeof(char)*size);
	str[0] ='\0'; //strcat needs a NUL to know where to concatinate
	for (i = start; i < numArgs; i++)
	{
		//Concatinate the current string from the list to the final string, and add a SEP if not the last one
		strcat(str, args[i]);
		if (i < numArgs-1)
		{
			strcat(str, sep);
		}
	}
	//Set the NUL terminating character
	str[size-1] = '\0';

	return str;
}

//Take the line and tokenize it (i.e. separate each argument into strings ending with '\0')
//Return the number list of char*/arguments
char** parseInput(char* line, int* numArgs)
{
	int argc = 0;
	char** args = (char**)malloc(sizeof(char*));
	char delims[] = {(char)SEP, '\0'}; //Separate by whitespace

	//Separate the initial command to start the tokenizer
	args[0] = strtok(line, delims);

	if (args[0] != NULL)
		argc++;
	char* val;
	while ((val = strtok(NULL, delims)) != NULL)
	{
		//If another argument was found and separated, increase the counter and resize the list, then add the new argument
		argc++;
		args = (char**)realloc(args, sizeof(char*)*argc);
		args[argc-1] = val;
	}
	//Convert the new line at the end of the string into a NUL, from when you enter the command as input
	val = args[argc-1];
	if (val[strlen(val)-1] == '\n')
	{
		val[strlen(val)-1] = '\0';
	}

	//Increment the argument list size by one and set end to NULL. The exec() function wants a NULL terminated list of args
	args = (char**)realloc(args, sizeof(char*)*(argc+1));
	args[argc] = NULL;

	//Set the passed in parameter to the argument count
	*numArgs = argc;

	return args;
}

//Print out the arguments provided
void echo(char* args[], const int size)
{
	int i;
	//Print out the arguments seperated by a space
	for (i = 1; i < size; i++)
	{
		printf("%s", args[i]);
		if (i < size-1)
		{
			printf(" ");
		}
	}
	printf("\n");
}


//Fork, wait and execute the command
void executeCommand(char* args[])
{
	pid_t p;
	int status;
	p = fork();
	//Error with fork
	if (p < 0)
	{
		perror("Could not fork child process...\n");
		exit(1);
	}
	else if (p == 0)
	{
		//In child process, execute the command and exit if unsuccessful
		if (execvp(args[0], args) < 0);
		{
			perror("Could not execute command: ");
			exit(1);
		}
	}
	else
	{
		//In parent process, wait for the child process and print status code if not 0
		waitpid(p, &status, 0);
		if (WIFEXITED(status))
		{
			int code = WEXITSTATUS(status);

			if (code != 0)
			{
				printf("Exit code %d\n", code);
			}
		}
	}

}

//Convert every special character in the command into its proper representation with the parity bit set
void setSpecial(char * line, int size)
{
	int i;
	for (i = 0; i < size; i++)
	{
		switch (line[i])
		{
			case ' ':
				line[i] = SEP;
				break;
			case '\'':
				line[i] = SQT;
				break;
			case '\"':
				line[i] = DQT;
				break;
			case '\\':
				line[i] = BST;
				break;
			case '$':
				line[i] = DOL;
				break;
			case '=':
				line[i] = EQ;
				break;
			case '(':
				line[i] = LPAR;
				break;
			case ')':
				line[i] = RPAR;
				break;
			case '&':
				line[i] = AMP;
				break;
			default:
				break;
		}
	}
}

//If a backslash is found, unspecialize the next character
void processBackSlash(char * line, int size)
{
	int s, d; //Source and Dest positions
	for (s = 0, d = 0; s < size; s++)
	{
		if (line[s] == (char)BST)
		{
			//If a backslash was found, set the dest to the next char as unspecialized.
			line[d] = line[s+1] & 127;
			s++; // Incrememnt the source pos so that the slash is ignored and the next char isn't processed twice
		}
		else
		{
			line[d] = line[s];
		}
		d++;
	}
	//Terminate the line with a NUL char
	line[d] = '\0';
}

//If a single quote is found that isn't inside double quotes,  unspecialize everything until the next single quote
void processSingleQuote(char * line, int size, int * procErr)
{
	// Source and Dest pos, bools for whether inside single or double quotes
	int s, d, insideSQT = 0, insideDQT = 0;
	for (s = 0, d = 0; s < size; s++)
	{
		//First check if a double quote is found because the single quote should remain if inside double quotes.
		//However, the inverse applies for double quotes, so make sure you're not in single quotes already
		if (insideSQT == 0 && line[s] == (char)DQT)
		{
			if (insideDQT == 0)
				insideDQT = 1;
			else
				insideDQT = 0;
		}
		//If not in double quotes and single quote is found, set the boolean to true and decrement the dest pos
		//so that the single quote has no effect
		if (insideDQT == 0 && line[s] == (char)SQT)
		{
			d--;
			if (insideSQT == 0)
				insideSQT = 1;
			else
				insideSQT = 0;
		}
		else if (insideSQT == 1)
		{
			//if inside single quotes, unspecialize everything
			line[d] = line[s] & 127;
		}
		else
		{
			//If not in single quotes, simply set the next char
			line[d] = line[s];
		}
		d++;
	}
	//NUL terminate the line
	line[d] = '\0';

	if (insideSQT == 1)
	{
		printf("Unmatched '\n");
		*procErr = 1;
	}
}

//This is executed after single quotes, back slashes and dollar signs have been processed
//At this point, if inside double quotes, unspecialize everything
void processDoubleQuote(char * line, int size, int * procErr)
{
	//Source and dest pos, bool for whether inside double quotes
	int s, d, inside = 0;
	for (s = 0, d = 0; s < size; s++)
	{
		//If a double quote is found, set the boolean to true and decrement the dest pos
		//so that the double quote has no effect
		if (line[s] == (char)DQT)
		{
			d--;
			if (inside == 0)
				inside = 1;
			else
				inside = 0;
		}
		else if (inside == 1)
		{
			//If in double quotes, unspecialize everything
			line[d] = line[s] & 127;
		}
		else
		{
			//If not in double quotes, simply set the next char
			line[d] = line[s];
		}
		d++;
	}
	//NUL terminate the line
	line[d] = '\0';

	if (inside == 1)
	{
		printf("Unmatched \"\n");
		*procErr = 1;
	}
}

//Insert a string into another one at position start. The end parameter is the position in source that should
//come right after the inserted string.
//i.e. source = "The purple dog", insert = "red", start = 4, end = 10. result would be "The red dog"
char * replaceIntoString(char* source, char* insert, int start, int end)
{
	// Number of chars is the sum of the two lengths + 1 for NUL, minus the difference between
	// end and start (the part from source being replaced)
	int size = strlen(source) + strlen(insert) - (end - start) + 1;
	char* newline = (char*)malloc(sizeof(char)*size);
	strncpy(newline, source, start);//copy beginning of source into newline, before the part being replaced
	newline[start] = '\0'; //add NUL for strcat to work
	strcat(newline, insert);
	//If there are any chars at or after end, concatenate them
	if (strlen(source+end) > 0)
		strcat(newline, source+end);

	return newline;
}

//Replace any instances of the dollar sign followed by a variable with its value retrieved from a hash table
char* processDol(Hashtable * h, char * line, int size, int * procErr)
{
	int s, d; //Source and dest pos
	for (s = 0, d = 0; s < size; s++)
	{
		if (line[s] == (char)DOL)
		{
			int pos = s;
			//TODO: must start with char
			// Find the position of the end of the variable after the dollar sign. i.e. for $var, pos would be 3
			while (isalpha(line[pos+1]) != 0 || isdigit(line[pos+1]) != 0)
			{
				pos++;
			}
			//Create a char array with the variable following the dollar sign
			char var[pos-s+1];
			strncpy(var, line+s+1, pos-s);
			var[pos-s] = '\0';

			//Look up the variable value and if found, replace the variable and '$' with its value
			Item* value = lookup(h, var);
			if (value != NULL)
			{
				//replaceIntoString allocates a new char*, so free the old line
				char* newline = replaceIntoString(line, value->value, s, pos+1);
				free(line);
				line = newline;
				size = strlen(line); //Reset size so that the for loop goes over the entire new string
				s--; //Decrement the source pos so that the newly insterted string can be processed
			}
			else
			{
				printf("Variable '%s' does not exist\n", var);
				*procErr = 1;
			}
		}
		else
		{
			line[d] = line[s];
			d++;
		}
	}

	//NUL terminate the line
	line[d] = '\0';

	return line;
}

// Surround the char at position pos with the string in surround.
// i.e. source = "test", surround = ' ', pos = 2. result would be "te s t"
char * surroundStringAt(char* source, char* surround, int pos)
{
	int surSize = strlen(surround);
	// Number of chars is size of source + 1 for NUL, plus 2*surround dize because it is being inserted on
	//  both sides of the char at pos
	int size = strlen(source) + surSize*2 + 1;
	char* newline = (char*)malloc(sizeof(char)*size);
	strncpy(newline, source, pos);//copy beginning of source into newline
	newline[pos] = '\0';
	// Add the surround contents, the char at pos, the surround contents again, then finish with the rest of source
	// The NUL char should be in surround for strcat to work
	strcat(newline, surround);
	strncat(newline, source+pos, 1);
	strcat(newline, surround);
	strcat(newline, source+pos+1);

	return newline;
}

// Surround the remaining special characters '=', '(', ')', and '&' with a SEP on both sides
char* separateSpecial(char * line, int size)
{
	char sep[2] = {(char)SEP, '\0'};
	int i;
	for (i = 0; i < size; i++)
	{
		//If one of the special chars is found, surround it with SEP
		if (line[i] == (char)EQ || line[i] == (char)LPAR || line[i] == (char)RPAR || line[i] == (char)AMP)
		{
			//Surround the special character and then free the old line char*
			char * newline = surroundStringAt(line, sep, i);
			free(line);
			line = newline;
			size = strlen(line);
			i++;

		}
	}

	return line;
}

// Call all of the command processing functions in the correct order and then return the processed line
char* processLine(char* line, int size, Hashtable * vars, int * procErr)
{

	setSpecial(line, size);
	processBackSlash(line, strlen(line));
	processSingleQuote(line, strlen(line), procErr);
	line = processDol(vars, line, strlen(line), procErr);
	processDoubleQuote(line, strlen(line), procErr);
	line = separateSpecial(line, strlen(line));

	return line;
}

// Given a list of hash table items (aliases), check if the given key is in the list and return a 1 if it is
int aliasWasUsed(Item** aliases, char* key, int size)
{
	int wasUsed = 0;
	for (int i = 0; i < size; i++)
	{
		if (strcmp(aliases[i]->key, key) == 0)
		{
			wasUsed = 1;
			break;
		}
	}

	return wasUsed;
}

int main(int argc, char* argv[])
{
	char* line = NULL; //The line to read from the user
	size_t len = 0; //Set to 0 for entire line
	ssize_t read; //Number of bytes read
	int numArgs = 0;
	int processingErr = 0; //boolean, set to 1 if there was an error processing the command (i.e. unmatched quote)
	int i;
	Hashtable * vars = make_hashtable(50);
	Hashtable * aliases = make_hashtable(50);

	printf("\nshahked_bleicher/ass_3/shell~> "); //Shell prompt

	while ((read = getline(&line, &len, stdin)) != -1)
	{
		processingErr = 0;
		//Process the command
		line = processLine(line, read, vars, &processingErr);
		//Tokenize the command
		char ** args = NULL;
		args = parseInput(line, &numArgs);

		//If the command isn't empty, either exec it or execute it manually as a shell command
		if (numArgs > 0)
		{
			/*
			*  Set all the variables for processing the aliases.
			*  aliasList: The list of aliases already processed, used to prevent infinite loop of aliases in command.
			*  alias: current alias being processed. Could probably just use the list directly, but didn't want to keep
			*  		  changing stuff after it was already already working
			*  nextAlias: where the first arg of the alias being processed is stored. i.e. for "alias test ls -a", nextAlias = "ls"
			*			  Didn't use strtok because it is destructive and changes the line itself.
			*  aliasLine: The string that will contain the final results for all of the processed aliases.
			*			  i.e. for "alias test ls"; "alias test2 test -a", the aliasLine should be "ls -a"
			*  size: Used to keep track of how many characters are in aliasLine, used when mallocing it
			*  pos: Used when getting the value for nextAlias. Points to the first SEP in the alias value, where the first arg ends
			*  aliasCount: The number of aliases processed so far, the size of aliasList
			*/
			Item* alias = NULL;
			Item** aliasList = (Item**)malloc(sizeof(Item*));
			aliasList[0] = NULL;
			char* nextAlias = NULL;
			char* aliasLine = NULL;
			int size = 0;
			char* pos = NULL;
			int aliasCount = 0;

			//Aliases can only be in the first arg, so use it as the key
			alias = lookup(aliases, args[0]);
			while (alias != NULL)
			{
				//printf("Alias Value is '%s'\n\n", alias->value);
				//Check if the current alias was is a repeat and break out if it is, setting aliasCount to -1 as an error flag
				if (aliasWasUsed(aliasList, alias->key, aliasCount) == 1)
				{
					aliasCount = -1;
					break;
				}
				//Resize the list of aliases and add the current one to the list
				aliasList = (Item**)realloc(aliasList, sizeof(Item*)*(aliasCount+1));
				aliasList[aliasCount] = alias;
				aliasCount++;
				// Set the pos char* to the first SEP in the alias value, right after the first arg
				pos = strchr(alias->value, (char)SEP);
				int length = 0;//Size of the first arg/possible next alias
				if (pos != NULL)
				{
					/*
					*  If there was a SEP, it means there's more after the first arg.
					*  Use pointer arithmetic to calculate the size of the first arg/token.
					*  Then set the value of nextAlias to that first argument and add to the total aliasLine size
					*/
					length = pos - alias->value;
					nextAlias = (char*)malloc(sizeof(char)*(length + 1));
					strncpy(nextAlias, alias->value, length);
					nextAlias[length] = '\0';
					size += strlen(pos);
				}
				else
				{
					/*
					*  pos is NULL, so no SEP was found and the first argument is the entire alias commmand.
					*  Just set nextAlias to the alias command value
					*/
					length = strlen(alias->value);
					nextAlias = (char*)malloc(sizeof(char)*(length + 1));
					strcpy(nextAlias, alias->value);
				}

				alias = lookup(aliases, nextAlias);
				if (alias == NULL)
				{
					/*
					*  If the next alias is NULL, then you need to include the first arg of the alias command
					*  into aliasLine because it is part of the actual command. i.e. "ls" is not an alias, so include its size as a command
					*/
					size += strlen(nextAlias);
				}
				free(nextAlias);
			}

			/*
			*  If there were any aliases, loop through them to set the final command from ther aliases
			*  Simply start at the last alias processed (at the deepest level) and append the command values
			*  to aliasLine.
			*/
			if (aliasCount > 0)
			{
				aliasLine = (char*)malloc(sizeof(char)*(size+1));
				aliasLine[0] = '\0';
				for (i = aliasCount-1; i >= 0; i--)
				{
					if (i == aliasCount-1)
					{
						//If you are on the deepest alias, just concat the alias command value
						strcat(aliasLine, aliasList[i]->value);
					}
					else
					{
						//Otherwise, concat everything after the first arg/SEP because it is another alias
						pos = strchr(aliasList[i]->value, (char)SEP);
						if (pos != NULL)
						{
							strcat(aliasLine, pos);
						}
					}
				}
				// temp holds all the already existing arguments, not including the first alias, as a SEP separated string
				char* temp = stringifyArgs(args, 1, numArgs);
				size = strlen(temp) + strlen(aliasLine);
				aliasLine = (char*)realloc(aliasLine, sizeof(char)*(size+2)); //+2 for SEP and NUL
				//Join the final alias command witht the rest of the initial args and process the result as a new command
				strcat(aliasLine, " ");
				strcat(aliasLine, temp);

				aliasLine = processLine(aliasLine, strlen(aliasLine), vars, &processingErr);
				free(temp);
				free(line); //line and args[0] point to the same address because args is just line after being tokenized
				free(args);
				free(aliasList);
				line = strdup(aliasLine);
				free(aliasLine);
				args = parseInput(line, &numArgs);
			}
			else if (aliasCount < 0)
			{
				printf("Alias Loop\n");
			}

			/*for (i = 0; i < numArgs; i++)
			{
				printf("args[%d] = '%s',  ", i, args[i]);
			}
			printf("\n");*/
			if (strcmp(args[0], "echo") == 0)
			{
				if (processingErr == 0)
				{
					echo(args, numArgs);
				}
			}
			else if(strcmp(args[0], "exit") == 0)
			{
				exit(0);
			}
			else if (strcmp(args[0], "set") == 0)
			{
				Item* item = NULL;
				if (numArgs >= 4 && args[2][0] == (char)EQ)
				{
					item = lookup(vars, args[1]);
					if (item == NULL)
					{
						add(vars, args[1], args[3]);
					}
					else
					{
						item->value = strdup(args[3]);
					}
				}
				else if (numArgs == 1)
				{
					list(vars);
				}
				else
				{
					printf("Incorrect Syntax Used, See Following: set [var_name]=[value]\n");
				}
			}
			else if (strcmp(args[0], "unset") == 0)
			{
				if (numArgs == 2)
				{
					removeItem(vars, args[1]);
				}
				else
				{
					printf("Incorrect Syntax Used, See Following: unset [var_name]\n");
				}
			}
			else if (strcmp(args[0], "alias") == 0)
			{
				if (numArgs >= 3)
				{
					alias = lookup(aliases, args[1]);
					char* command = stringifyArgs(args, 2, numArgs);
					if (alias == NULL)
					{
						add(aliases, args[1], command);
					}
					else
					{
						alias->value = strdup(command);
						free(command);
					}
				}
				else if (numArgs == 1)
				{
					printf("All Aliases:\n");
					list(aliases);
				}
				else
				{
					printf("Incorrect Syntax Used, See Following: alias [name][ ][command]\n");
				}
			}
			else if (strcmp(args[0], "unalias") == 0)
			{
				if (numArgs == 2)
				{
					removeItem(aliases, args[1]);
				}
				else
				{
					printf("Incorrect Syntax Used, See Following: unset [var_name]\n");
				}
			}
			else if (aliasCount >= 0)
			{
				//If it's not a shell command and there was not an alias loop, execute the command
				if (processingErr == 0)
				{
					executeCommand(args);
				}
			}
		}
			// Don't need to free each arg because it is the same memory as the line char*, which is reallocated by getline()
		free(line);
		line = NULL;
		free(args);

		printf("\nshahked_bleicher/ass_3/shell~> ");
	}


	free(line);

	return 0;
}
