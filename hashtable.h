#ifndef HASH_TABLE_H
#define HASH_TABLE_H

typedef struct Item
{
	char * key, * value;
} Item;

typedef struct Link
{
	Item* data;
	struct Link* next;
} Link;

typedef struct
{
	int size;
	Link** table;
} Hashtable;

// k & v will be strdupped, controversial
Item * make_item(const char *k, const char *v);

Link * make_link(Item * d, Link * n);

Hashtable* make_hashtable(int sz);

int hash(const char * s, int size);

Item* lookup(Hashtable * h, const char* k);

void* add(Hashtable * h, const char* k, char* v);

void* removeItem(Hashtable * h, const char* k);

void* list(Hashtable * h);

#endif
