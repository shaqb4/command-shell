#include "hashtable.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

// k & v will be strdupped, controversial
Item * make_item(const char *k, const char *v)
{
	Item * i = (Item *)malloc(sizeof(Item));
	i->key = strdup(k);
	i->value = strdup(v);
	return i;
}

Link * make_link(Item * d, Link * n)
{
	Link* l = (Link *)malloc(sizeof(Link));
	l->data = d;
	l->next = n;
	
	return l;
}

Hashtable* make_hashtable(int sz)
{
	Hashtable* h = (Hashtable*)malloc(sizeof(Hashtable));
	h->size = sz;
	h->table = (Link**)malloc(sizeof(Link*)*sz);
	bzero(h->table, sizeof(Link*)*sz);
	
	return h;
}

int hash(const char * s, int size)
{
	//unsigned so that modulo at end doesn't become negative
	unsigned int i, h = 12379;
	for (i = 0; s[i] != '\0'; i++)
	{
		h = h*167 + s[i];
	}
	
	return h%size;
}

Item* lookup(Hashtable * h, const char* k)
{
	int pos = hash(k, h->size);
	Link* ptr = h->table[pos];
	Item* i = NULL;
	while (ptr != NULL)
	{
		if (strcmp(ptr->data->key, k) == 0)
		{
			i = ptr->data;
			break;
		}
		
		ptr = ptr->next;
	}
	
	return i;
}


void* add(Hashtable * h, const char* k, char* v)
{
	int pos = hash(k, h->size);
	Link* ptr = h->table[pos];
	Item* i = NULL;
	while (ptr != NULL)
	{
		if (strcmp(ptr->data->key, k) == 0)
		{
			ptr->data->value = strdup(v);
			i = ptr->data;
			break;
		}
		
		ptr = ptr->next;
	}
	if (i == NULL)
	{
		h->table[pos] = make_link(make_item(k, v), h->table[pos]);
	}
}

void* removeItem(Hashtable * h, const char* k)
{
	int pos = hash(k, h->size);
	Link* ptr = h->table[pos];
	Link* next = NULL;
	while (ptr != NULL)
	{
		if (ptr->next == NULL && strcmp(ptr->data->key, k) == 0)
		{
			next = ptr;
			h->table[pos] = NULL;
			free(next->data->key);
			free(next->data->value);
			free(next->data);
			free(next);
			break;
		}
		else if (strcmp(ptr->next->data->key, k) == 0)
		{
			next = ptr->next;
			ptr->next = ptr->next->next;
			free(next->data->key);
			free(next->data->value);
			free(next->data);
			free(next);
			break;
		}
	}
}

void * list(Hashtable * h)
{
	for (int i = 0; i < h->size; i++)
	{
		Link* ptr = h->table[i];
		while (ptr != NULL)
		{
			printf("%s => %s\n", ptr->data->key, ptr->data->value);
			ptr = ptr->next;
		}
	}
}
