# Basic Shell #

A command line shell that I completed for a Systems Programming course.

## How to Use

Run the program by running the shell command with no arguments.

alias – List all of your existing aliases

alias {name} {command} – Set an alias by adding it to the hashtable

unalias – Delete an alias by removing it from the hashtable

set – List all of your existing shell variables

set {name} = {command} – Set a variable by adding it to the hashtable

unset – Delete a variable by removing it from the hashtable

echo – Display the arguments as a space separated string, correctly handling quotes and variables.

command - If not mentioned in the above list, the command is treated as a normal unix command and is executed in another process

## Compiling
cc main.c hashtable.c -o shell

If that does not work, you may need to add "-std=gnu99" at the end.

During development, I had no issue compiling with the first command. However, now that I've gone back to test it, it complains about being outside of c99 mode and some functions being implicitly declared. I am unsure what changed, whether it's the environment or somehow the code itself, but stating that it is the gnu99 standard seems to work.